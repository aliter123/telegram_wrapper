import os
import asyncio
import io
import json

from telethon import TelegramClient, events, sync
from telethon.sessions import StringSession

from tornado import escape, web, ioloop, locks, httpserver
from tornado.options import define, options, parse_command_line

define("port", default=5555, help="run on the given port", type=int)


auth_client = client = TelegramClient(
    StringSession(), os.environ.get("API_ID"), os.environ.get("API_HASH")
)


class InvalidTokenException(Exception):
    """Custom Exception Identifier for Auth"""

    pass


class BaseHandler(web.RequestHandler):
    """Base handler to manage common api flow"""

    def prepare(self):
        """Validate auth token"""

        auth_token = self.request.headers.get("Authorization")
        if auth_token != self.settings.get("auth_token"):
            raise InvalidTokenException("Invalid or no token")

        return super().prepare()

    def check_origin(self, origin):

        """Validate CORS"""
        return True

    def set_default_headers(self):
        self.set_header("Content-Type", "application/json")

    def write_error(self, status_code, **kwargs):
        """ Gracefully handle fallbacks """

        self.write(
            {
                "success": False,
                "message": "It's all Boomed!",
                "context": str(kwargs.get("exc_info")[1]),
            }
        )


class UserSession(BaseHandler):

    SUPPORTED_METHODS = ("GET", "POST")

    async def prepare(self):
        """Initialize telegram client"""

        super().prepare()

        await auth_client.connect()

        if await auth_client.is_user_authorized():
            await client.log_out()
            await auth_client.connect()

    async def get(self):
        # Request token for user

        phone = self.get_argument("phone")

        response = await auth_client.send_code_request(phone)

        self.write({"success": True, "phone_code_hash": response.phone_code_hash})

    async def post(self):
        # Fetch user session

        phone = self.get_argument("phone")

        code = self.get_argument("code")
        phone_code_hash = self.get_argument("phone_code_hash")

        session = await auth_client.sign_in(
            phone=phone, code=code, phone_code_hash=phone_code_hash
        )

        user_session = str(auth_client.session.save())
        await client.log_out()

        self.write({"success": True, "user_session": user_session})


class SendMessage(BaseHandler):

    SUPPORTED_METHODS = ("POST",)

    async def post(self):
        """ Process Request """

        client_session = self.get_argument("client_session")
        self.entities = self.get_arguments("entities")

        if not self.entities:
            self.set_status(400)
            self.write({"success": False, "message": "valid entities are required"})
            return

        self.message = self.get_argument("message", default=None, strip=True)

        self.media_file = self.request.files.get("media_file")

        self._client = TelegramClient(
            StringSession(client_session),
            self.settings.get("api_id"),
            self.settings.get("api_hash"),
        )
        await self._client.start()

        if self.media_file:
            await self.send_file()
        else:
            await asyncio.wait(
                [
                    self._client.send_message(entity, self.message, link_preview=False)
                    for entity in self.entities
                ]
            )

        await self._client.disconnect()
        self.write({"success": True})

    async def send_file(self, *args, **kwargs):

        # pass reference to uploaded file for each send_file
        sent = await asyncio.wait(
            [
                self._client.send_file(
                    entity,
                    (f.get("body") for f in self.media_file),
                    silent=False,
                    caption=self.message,
                    # force_document=True,
                )
                for entity in self.entities
            ]
        )
        return sent


class Application(web.Application):
    """Default Application Settings"""

    def __init__(self):
        handlers = [
            (r"/send-message", SendMessage),
            (r"/user-session", UserSession),
        ]
        settings = dict(
            cookie_secret=None,
            xsrf_cookies=False,
            debug=False,
            api_id=os.environ.get("API_ID"),
            api_hash=os.environ.get("API_HASH"),
            auth_token=os.environ.get("AUTH_TOKEN"),
        )
        web.Application.__init__(self, handlers, **settings)


def main():
    parse_command_line()

    http_server = httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
